
import './App.css';
import React, { Component } from 'react';
import Index from "./components/servicios/Index";
import Crear from "./components/servicios/Crear";
import Login from "./components/Auth/Login";
import { Container, Button } from 'react-bootstrap';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      mostrarCrear: false,
      mostrarIndex: true,
      estaLogueado: false,
      usuario: {}
    }

    this.mostrarFormulario = this.mostrarFormulario.bind(this);
    this.mostrarIndex = this.mostrarIndex.bind(this);
    this.cerrarSesion = this.cerrarSesion.bind(this);

  }


  componentDidMount() {
    let usuarioSt = localStorage.getItem('usuario');
    if(usuarioSt){
      this.setState({
        estaLogueado:true,
        usuario:JSON.parse(usuarioSt)
      })
    }
    
  }

  mostrarFormulario() {
    this.setState({
      mostrarCrear: true,
      mostrarIndex: false
    })
  }

  mostrarIndex() {

    this.setState({
      mostrarCrear: false,
      mostrarIndex: true
    })
  }

  cerrarSesion() {

    localStorage.removeItem('token')
    localStorage.removeItem('usuario')
    window.location.replace('');
  }

  render() {
   
    return (
      <Container className="mt-5 row cont mx-5" >
        <div className="col-12 mt-5">

          {this.state.estaLogueado ? '' : <Login></Login>}
          {this.state.mostrarIndex && 
          this.state.estaLogueado ? <Index mostrarIndex={this.mostrarIndex} usuarioRol={this.state.usuario.rol}></Index> : ''}
          {this.state.mostrarCrear && 
          this.state.estaLogueado &&
          this.state.usuario.rol === 1 ? <Crear mostrarIndex={this.mostrarIndex}></Crear> : ''}
        </div>

        {this.state.mostrarIndex && 
        this.state.estaLogueado &&
        this.state.usuario.rol === 1 ?
          <div className="col-12 mt-5">
            <Button onClick={this.mostrarFormulario} className="mx-4">  Crear Servicio </Button>
            <Button variant="danger" className="mx-5" onClick={this.cerrarSesion}>  Cerrar Sesion </Button>
          </div>
          : ''}

      </Container>
    );
  }
}

export default App;

