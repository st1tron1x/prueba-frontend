import React, { Component } from 'react';
import axios from 'axios';
import { Form, Button} from 'react-bootstrap'

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            contrasena:''      

        }
        
        this.mostrarIndex = this.mostrarIndex.bind(this);
        this.iniciarSesion = this.iniciarSesion.bind(this);
        this.handleChangeUsername = this.handleChangeUsername.bind(this);
        this.handleChangeContrasena = this.handleChangeContrasena.bind(this);


    }

    componentDidMount() {

       

    }

    handleChangeUsername(event) {
        const value = event.target.value;        
        this.setState({username: value});
    }
    handleChangeContrasena(event) {
        const value = event.target.value;        
        this.setState({contrasena: value});
    }

    iniciarSesion(){

        axios.post('http://localhost:3001/api/login',
        {
            username:this.state.username,
            contrasena:this.state.contrasena
        })
        .then(res=>{

            
            localStorage.setItem('usuario',JSON.stringify(res.data.usuario));
            localStorage.setItem('token',res.data.access_token);
            window.location.replace('');
        }).catch(err=>{
            console.log(err);
        })

    }

  
    mostrarIndex() {
        this.setState({
            mostrarEditar: false
        })
    }


    render() {
        return (

            <div className="container">
                <Form>
                    <Form.Group className="mb-3" controlId="formBasicnombre">
                        <Form.Label>username</Form.Label>
                        <Form.Control type="text" placeholder="Ingrese username" name="username" value={this.state.username} onChange={this.handleChangeUsername} />
                    </Form.Group>

                    <Form.Group className="mb-3" controlId="formBasicContraseña">
                        <Form.Label>Contraseña</Form.Label>
                        <Form.Control type="password" placeholder="Contraseña" name="Contrasena" value={this.state.contrasena} onChange={this.handleChangeContrasena} />
                    </Form.Group>
                </Form>
                <Button onClick={this.iniciarSesion} variant="primary" type="button">Iniciar Sesion</Button>

            </div>

        );
    }

}

export default Login;