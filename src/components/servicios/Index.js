import React, { Component } from 'react';
import axios from 'axios';
import { ListGroup, Button } from 'react-bootstrap'
import Editar from "./Editar";

class Index extends Component {
    constructor(props) {
        super(props);
        this.state = {
            listaServicio: [],
            idServicio: 0,
            servicio: {},
            mostrarEditar: false
        }
        this.eliminarServicio = this.eliminarServicio.bind(this);
        this.obtenerServicios = this.obtenerServicios.bind(this);
        this.editarServicio = this.editarServicio.bind(this);
        this.mostrarIndex = this.mostrarIndex.bind(this);


    }

    componentDidMount() {

        let token = localStorage.getItem('token');


        axios.get('http://localhost:3001/api/servicio',
            {
                headers: {
                    authorization: 'Bearer ' + token
                }
            })
            .then(res => {
                this.setState({
                    listaServicio: res.data
                })
            }).catch(err => {
                console.log(err);
            })

    }

    obtenerServicios() {
        let token = localStorage.getItem('token');
        axios.get('http://localhost:3001/api/servicio', {
            headers: {
                Authorization: 'Bearer ' + token
            }
        })
            .then(res => {
                this.setState({
                    listaServicio: res.data
                })
            }).catch(err => {
                console.log(err);
            })
    }

    listItems() {
        
        return this.state.listaServicio.map((lista) =>
            <ListGroup.Item key={lista.id}>
                <div className='row'>
                    <div className='col-4'>{lista.nombre}</div>
                    <div className='col-4'>{lista.telefono}</div>
                    {this.props.usuarioRol === 1 ?
                        <div className='col-2'><Button onClick={() => this.eliminarServicio(lista.id)} variant="primary" type="button">Eliminar</Button></div>
                        : ''}
                    {this.props.usuarioRol === 1 ?
                        <div className='col-2'><Button onClick={() => this.editarServicio(lista.id, lista)} variant="primary" type="button">Editar</Button></div>
                        : ''}
                </div>
            </ListGroup.Item>
        );
    }
    eliminarServicio(id) {
        let token = localStorage.getItem('token');
        axios.delete('http://localhost:3001/api/servicio/' + id, {
            headers: {
                Authorization: 'Bearer ' + token
            }
        })
            .then(res => {
                this.obtenerServicios();
            }).catch(err => {
                console.log(err);
            })
    }

    editarServicio(id, lista) {

        this.setState({
            mostrarEditar: true
        })
        this.setState({
            idServicio: id
        })
        this.setState({
            servicio: lista
        })

    }
    mostrarIndex() {
        this.setState({
            mostrarEditar: false
        })
    }


    render() {
        return (

            <div className="container">
                {this.state.mostrarEditar ? <Editar obtenerServicios={this.obtenerServicios} mostrarIndex={this.mostrarIndex} id={this.state.idServicio} servicio={this.state.servicio}> </Editar> :
                    <ListGroup>
                        {this.listItems()}
                    </ListGroup>
                }

            </div>

        );
    }

}

export default Index;