import React, { Component } from 'react';
import axios from 'axios';
import { Form, Button } from 'react-bootstrap'

class Crear extends Component {
    constructor(props) {
        super(props);
        this.state = {
            nombre: '',
            telefono: '',
        }

        this.handleChangeNombre = this.handleChangeNombre.bind(this);
        this.handleChangeTelefono = this.handleChangeTelefono.bind(this);
        this.crearServicio = this.crearServicio.bind(this);
        this.volverIndex = this.volverIndex.bind(this);

    }

    handleChangeNombre(event) {
        const value = event.target.value;        
        this.setState({nombre: value});
    }
    handleChangeTelefono(event) {
        const value = event.target.value;        
        this.setState({telefono: value});
    }


    crearServicio() {
        let token = localStorage.getItem('token');
        axios.post('http://localhost:3001/api/servicio', {
            nombre: this.state.nombre,
            telefono: this.state.telefono,
        },{
            headers:{
                Authorization : 'Bearer ' + token
            }
        }).then(res => {

            console.log(res.data);

        }).catch(err => {
            console.log(err);
        })
    }

    volverIndex(){
        this.props.mostrarIndex();
    }


    render() {
        return (

            <div>
                <Form>
                    <Form.Group className="mb-3" controlId="formBasicnombre">
                        <Form.Label>Nombre</Form.Label>
                        <Form.Control type="nombre" placeholder="Enter name" name="nombre" value={this.state.nombre} onChange={this.handleChangeNombre} />
                    </Form.Group>

                    <Form.Group className="mb-3" controlId="formBasictelefono">
                        <Form.Label>Telefono</Form.Label>
                        <Form.Control type="telefono" placeholder="telefono" name="telefono" value={this.state.telefono} onChange={this.handleChangeTelefono} />
                    </Form.Group>
                </Form>
                <Button onClick={this.crearServicio} variant="primary" type="button">Guardar</Button>
                <Button onClick={this.volverIndex} variant="primary" type="button">Volver</Button>
            </div>


        );
    }
}

export default Crear;