import React, { Component } from 'react';
import axios from 'axios';
import { Form, Button } from 'react-bootstrap'

class Editar extends Component {
    constructor(props) {
        super(props);
        this.state = {
            nombre: this.props.servicio.nombre,
            telefono:this.props.servicio.telefono,
            
        }

        this.handleChangeNombre = this.handleChangeNombre.bind(this);
        this.handleChangeTelefono = this.handleChangeTelefono.bind(this);      
        this.volverIndex = this.volverIndex.bind(this);
        this.editarServicio = this.editarServicio.bind(this);
        this.limpiaCampo = this.limpiaCampo.bind(this);


    }

    componentDidMount() {

      

    }

    handleChangeNombre(event) {
        const value = event.target.value;        
        this.setState({nombre: value});
    }
    handleChangeTelefono(event) {
        const value = event.target.value;        
        this.setState({telefono: value});
    }



    editarServicio(id) {
        let token = localStorage.getItem('token');
        axios.put('http://localhost:3001/api/servicio/'+id,       
        {
            nombre: this.state.nombre,
            telefono: this.state.telefono,
        },{
            headers:{
                Authorization : 'Bearer ' + token
            }
        },).then(res => {
                this.props.obtenerServicios();
                this.props.mostrarIndex();

        }).catch(err => {
            console.log(err);
        })
    }

    volverIndex(){
        this.props.mostrarIndex();
    }

    limpiaCampo() {
        
    }

    render() {
        return (
            
            <div>
                <Form>
                    <Form.Group className="mb-3" controlId="formBasicnombre">
                        <Form.Label>Nombre</Form.Label>
                        <Form.Control type="nombre" placeholder="Enter name" name="nombre" value={this.state.nombre} onChange={this.handleChangeNombre} />
                    </Form.Group>

                    <Form.Group className="mb-3" controlId="formBasictelefono">
                        <Form.Label>Telefono</Form.Label>
                        <Form.Control type="telefono" placeholder="telefono" name="telefono" value={this.state.telefono} onChange={this.handleChangeTelefono} />
                    </Form.Group>
                </Form>

                <Button onClick={this.volverIndex} variant="primary" type="button">Volver</Button>
                <Button className="mx-5" onClick={()=>this.editarServicio(this.props.id)} variant="primary" type="button">Guardar</Button>
            </div>


        );
    }
}

export default Editar;